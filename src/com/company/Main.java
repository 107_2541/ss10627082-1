package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        java.util.Scanner scanner =
                new java.util.Scanner(System.in);

        System.out.print("輸入整數:");
        int input = scanner.nextInt();

        if(input % 2 == 0) // 如果餘數為 0
            System.out.println(input + " 是偶數");
        if(input % 2 != 0) // 如果餘數不為 0
            System.out.println(input + " 是奇數");
    }
}